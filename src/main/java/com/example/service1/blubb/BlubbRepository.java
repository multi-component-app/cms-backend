package com.example.service1.blubb;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface BlubbRepository extends PagingAndSortingRepository<Blubb, Long> {

}
